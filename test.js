const express = require('express');
const app = express();
const port = 3402;

// Set up a route to serve the HTML form
app.get('/', (req, res) => {
    res.send(`
        <form action="/submit" method="post">
            <label for="name">Name:</label>
            <input type="text" id="name" name="name"><br><br>
            <label for="email">Email:</label>
            <input type="email" id="email" name="email"><br><br>
            <label for="phone">Phone:</label>
            <input type="tel" id="phone" name="phone"><br><br>
        </form>
    `);
});

app.post('/submit', (req, res) => {
    const { name, email } = req.body;
    res.send(`Form submitted! Name: ${name}, Email: ${email}`);
});

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
